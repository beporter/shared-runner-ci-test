# shared-runner-ci-test

Testing various configurations for GitLab.com's shared CI runners in relation to these LetsEncrypt root certificate issues:

- https://gitlab.com/gitlab-org/gitlab/-/issues/339842
- https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28238